const fs = require('fs')
const getUuid = require('uuid-by-string')
const merge = require('lodash.merge')

fs.readFile('tflint-output.json', 'utf8', function (err, data) {
  if (err) {
    return console.log(err)
  }

  const tflintObj = JSON.parse(data)

  let cqArr = tflintObj.issues.map((issue) => {
    return {
      description: issue.message,
      fingerprint: getUuid(JSON.stringify(issue)),
      severity: (
        (issue.rule.severity == "warning") ? "minor" :
        (issue.rule.severity == "error") ? "blocker" :
        "unknown"
      ),
      location: {
        path: issue.range.filename,
        lines: {
          begin: issue.range.start.line
        }
      }
    }
  })

  // Generate the code quality artifact.
  fs.writeFile('tflint-cq.json', JSON.stringify(cqArr), function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('Code quality artifact from `tflint` saved to tflint-cq.json')
  })

  // Merge results from existing artifacts.
  fs.readFile('terraform-cq.json', 'utf8', function (err, data) {
    if (err) {
      return console.log(err)
    }

    fs.writeFile('gl-code-quality-report.json', JSON.stringify(merge(JSON.parse(data), cqArr)), function (err) {
      if (err) {
        return console.log(err)
      }
  
      console.log('Code quality artifacts from `terraform` and `tflint` merged into gl-code-quality-report.json')
    })
  })
})
