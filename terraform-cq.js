const fs = require('fs')
const getUuid = require('uuid-by-string')

fs.readFile('terraform-output.json', 'utf8', function (err, data) {
  if (err) {
    return console.log(err)
  }
  
  const terraformObj = JSON.parse(data)

  let cqArr = terraformObj.diagnostics.map((diagnosis) => {
    if (diagnosis.range) {
      return {
        description: diagnosis.summary + " (" + diagnosis.detail + ")",
        fingerprint: getUuid(JSON.stringify(diagnosis)),
        severity: (
          (diagnosis.severity == "warning") ? "minor" :
          (diagnosis.severity == "error") ? "blocker" :
          "unknown"
        ),
        location: {
          path: diagnosis.filename,
          lines: {
            begin: diagnosis.range.start.line
          }
        }
      }
    }
  })

  // Generate the code quality artifact.
  fs.writeFile('terraform-cq.json', JSON.stringify(cqArr), function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('Code quality artifact from `terraform` saved to terraform-cq.json')
  })
})
