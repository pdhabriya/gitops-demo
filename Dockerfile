FROM golang:1.15-alpine3.12 AS terraform-bundle

# Use only trusted apk mirrors.
COPY config/apk-repositories.txt /etc/apk/repositories

# Install dependencies for terraform-bundle.
RUN \
  apk add -uv --no-progress --no-cache \
    curl \
    jq \
    tar \
    unzip

# Build Terraform, but first, copy over providers.
COPY config/terraform-build-deps.hcl .

# Build Terraform.
RUN \
  curl -SsL https://github.com/hashicorp/terraform/archive/v0.13.3.tar.gz -o terraform-src.tar.gz && \
  mkdir -p /go/src/github.com/hashicorp/ && \
  tar -C /go/src/github.com/hashicorp/ -xzf terraform-src.tar.gz && \
  mv /go/src/github.com/hashicorp/terraform*/ /go/src/github.com/hashicorp/terraform/ && \
  go install /go/src/github.com/hashicorp/terraform/tools/terraform-bundle/ && \
  terraform-bundle package -os=linux -arch=amd64 terraform-build-deps.hcl && \
  mkdir -p terraform-bundle/ && \
  unzip -d terraform-bundle/ terraform_*.zip

################################################################################

FROM alpine:3.12

# Set USER, because Alpine doesn't.
RUN export USER=$(whoami)

# Use only trusted apk mirrors.
COPY config/apk-repositories.txt /etc/apk/repositories

# Install the necessities.
RUN \
  apk add -uv --no-progress --no-cache \
    curl \
    jq \
    unzip

# Install git.
RUN \
  apk add -uv --no-progress --no-cache \
    git \
    git-lfs && \
  git lfs install

# Install Terraform.
COPY --from=terraform-bundle /go/terraform-bundle/* /usr/local/bin/
COPY config/terraformrc.ini /root/.terraformrc
RUN chmod +x /usr/local/bin/terraform*

# Install gitlab-terraform.
RUN \
  curl -SsL https://gitlab.com/gitlab-org/terraform-images/-/raw/19107a27c5017f98ada47c8fb63c62da2f0fc87d/src/bin/gitlab-terraform.sh -o /usr/local/bin/gitlab-terraform && \
  chmod +x /usr/local/bin/gitlab-terraform

# Install tflint
RUN \
  curl -L "$(curl -Ls https://api.github.com/repos/terraform-linters/tflint/releases/latest | grep -o -E "https://.+?_linux_amd64.zip")" -o tflint.zip && unzip tflint.zip && rm -f tflint.zip && \
  mv tflint /usr/local/bin/tflint && \
  chmod +x /usr/local/bin/tflint

# Install tfsec.
RUN \
  curl -SsL $(curl -Ss https://api.github.com/repositories/173785481/releases/latest | jq -r ".assets[] | select(.name | test(\"tfsec-linux-amd64\")) | .browser_download_url") -o /usr/local/bin/tfsec && \
  chmod +x /usr/local/bin/tfsec

# Install Ansible.
RUN apk add -uv --no-progress --no-cache ansible

# Install Node.js and npm.
COPY config/npmrc.ini /usr/etc/npmrc
RUN \
  apk add -uv --no-progress --no-cache nodejs npm && \
  npm install -g --no-progress npm

# Install and initialize artifact builders.
RUN mkdir -p /opt/gitlab-terraform-artifact-builder/
COPY package.json /opt/gitlab-terraform-artifact-builder/package.json
COPY terraform-cq.js /opt/gitlab-terraform-artifact-builder/terraform-cq.js
COPY tflint-cq.js /opt/gitlab-terraform-artifact-builder/tflint-cq.js
COPY tfsec-sast.js /opt/gitlab-terraform-artifact-builder/tfsec-sast.js
RUN \
  cd /opt/gitlab-terraform-artifact-builder/ && \
  npm install
