# How to contribute

1. From the [upstream project](https://gitlab-core.us.gitlabdemo.cloud/community/project-templates/gitops-demo), create an issue if one for your contribution doesn't already exist. Apply all applicable labels.
2. From the [Issue Board](https://gitlab-core.us.gitlabdemo.cloud/community/project-templates/gitops-demo/-/boards/3263), drag the issue to the ~"in progress" list and assign it to yourself.
3. Create a Merge Request via the issue.
4. Contribute!
