# GitLab GitOps Demo

This project can be used to demonstrate a GitOps pipeline that showcases features of Ultimate. It follows a similar workflow to a typical day-in-the-life development demo while integrating best of breed tools for Infrastructure as Code and configuration management.

This is a fairly concise guide with important information regarding setup, security, and operation; please read it in full.

> Additional supporting documentation can be found in the [Demo Library issue](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/demo-catalog/-/issues/29) for the GitOps Demo.

## Quick-start guide

1. Create a subgroup specifically for this demo.
2. **Export** the [upstream project](https://gitlab-core.us.gitlabdemo.cloud/community/project-templates/gitops-demo) and import it as a new project in your GitOps subgroup. (Forking the repository will exclude important project configurations necessary for proper operation.)
3. Manually run a pipeline on the main branch to build your builder image.
5. Obtain an [individual AWS account](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started) (if you don't have one already).
6. While logged into the AWS console, navigate to the [CentOS AMI Marketplace page](https://aws.amazon.com/marketplace/pp?sku=aw0evgkw8e5c1q413zgy5pjce) and click on the "Continue to Subscribe" call-to-action in the top-right. Follow the prompts to accept the terms of service.
7. Set the following CI/CD variables via your subgroup by navigating to _Settings → CI/CD → Variables_:

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_DEFAULT_REGION`

> These variables will be shared by AWS CLI and Terraform. You're advised to create an ad hoc, least-privilege user in your AWS account with only the permissions needed to deploy the resources in your Terraform templates. Access to your subgroup and the variable for your AWS secret access key should be restricted. In a future iteration, [support for user credentials will be deprecated](https://gitlab.com/marquard/gitlab-iac-demo/-/issues/45).

### Performing the demo

The GitOps Demo follows a similar workflow to the typical day-in-the-life demo. Follow the typical [GitLab developer workflow](https://about.gitlab.com/topics/version-control/what-is-gitlab-workflow/) to perform the demo. You might consider making a purposeful change to the [web app](html/index.html) as part of your demo to trigger a deployment pipeline and see your change reflected in the review environment it creates.

## Updating

> This demo is officially supported and updated regularly. You're encouraged to check back regularly and update to the latest available version.

1. **Export** the [upstream project](https://gitlab-core.us.gitlabdemo.cloud/community/project-templates/gitops-demo) and import it as a new project in your GitOps subgroup.
2. Manually run a pipeline on the main branch to build your builder image.
3. [Perform a test demo](#performing-the-demo) to ensure proper operation.
4. Delete the project in your GitOps subgroup containing an older release of the demo.

## Components of the project

### Builder image

This project contains pipelines for building and supporting your own builder image that will be used for running GitOps pipelines. Although the image must be built on a Runner with Internet access, the image itself can be run in [offline environments](https://docs.gitlab.com/ee/user/application_security/offline_deployments/) for demos that might need to be delivered in a SCIF.

#### Customizing your builder image

To iterate on your builder image, use the ~"dev:image" label in your Merge Request. If you create an MR from an issue that has this label, your MR will inherit the label. This will run an image build job on MR events, pushing to your container registry with a tag consistent with the name of the branch. After merging your changes into the main branch run a manual pipeline on the main branch to release the image, pushing to the container registry with the `latest` tag.

### GitOps pipeline

With a typical Merge Request, a GitOps pipeline will run with each `git push` event. Terraform will deploy infrastructure to AWS before handing off to Ansible for configuration.

### Web app

The GitOps pipeline will deploy a simple web app in a review environment. This [web app](html/) can be customized to your liking, but if your changes could benefit the upstream demo, consider [contributing](#how-to-contribute) to the upstream project.

## How to contribute

Everyone is encouraged to contribute! There are a [backlog](https://gitlab-core.us.gitlabdemo.cloud/community/project-templates/gitops-demo/-/boards/3263) of issues. See [CONTRIBUTING.md](CONTRIBUTING.md) for direction on how to contribute to the demo.
